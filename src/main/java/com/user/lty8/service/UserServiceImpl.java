package com.user.lty8.service;

import com.user.lty8.dao.entity.User;
import com.user.lty8.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    public User addUser(User user){
        return repository.save(user);
    }

    public Optional<User> findById(Integer id){
        return repository.findById(id);
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    public List<User> getAllUserList(){
        return repository.findAll();
    }
}
