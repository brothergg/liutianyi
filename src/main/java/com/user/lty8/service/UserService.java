package com.user.lty8.service;

import com.user.lty8.dao.entity.User;
import com.user.lty8.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    List<User> getAllUserList();


}
