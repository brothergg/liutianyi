package com.user.lty8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lty8Application {

    public static void main(String[] args) {
        SpringApplication.run(Lty8Application.class, args);
    }
}
