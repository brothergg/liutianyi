package com.user.lty8.controller;

import com.user.lty8.dao.entity.User;
import com.user.lty8.dto.RequestUser;
import com.user.lty8.dto.ResponseUser;
import com.user.lty8.exception.ResourceNotFoundException;
import com.user.lty8.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl service;

    @PostMapping(path="/add")
    public ResponseUser addUser(@RequestBody RequestUser requestUser) {
        User user = new User();
        user.setAge(requestUser.getAge());
        user.setName(requestUser.getName());

        User resEntityUser = service.addUser(user);
        ResponseUser resUser = new ResponseUser();
        resUser.setAge(resEntityUser.getAge());
        resUser.setClassName("Math class");
        resUser.setName(resEntityUser.getName());

        return resUser;
    }

    @GetMapping(path="/find_by_id")
    public User findById(@RequestParam Integer id){
        Optional<User> userOpt = service.findById(id);
        if(userOpt.isPresent()){
            return userOpt.get();
        }else{
            throw new ResourceNotFoundException("user","id",id);
        }
    }

    @DeleteMapping(path="/delete_by_id")
    public User userId(@RequestParam Integer id){
        Optional<User> userOpt = service.findById(id);
        if(userOpt.isPresent()){
            service.deleteById(id);
            return userOpt.get();
        }else{
            throw new ResourceNotFoundException("user","id",id);
        }

    }

    @GetMapping(path="/find_all")
    public List<User> findAll(){
        return service.getAllUserList();
    }
}
